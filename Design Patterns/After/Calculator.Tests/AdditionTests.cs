﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Classes;
using Xunit;

namespace Calculator.Tests
{
    public class AdditionTests
    {
        [Theory]
        [InlineData(1,2,5)]
        [InlineData(1,1,2)]
        [InlineData(1,3,4)]
        [InlineData(1,4,7)]
        public void TwoNumbersAreAdded_TheyAreValid_CorrectSumIsReturned(double num1, double num2, double expectedResult)
        {
            //Arrange
            var sut = new Addition();
            //Act
            sut.SetNum1(num1);
            sut.SetNum2(num2);
            var actualResult = sut.GetResult();

            //Assert
            Assert.Equal(expectedResult, actualResult);
        }

        [Fact]
        public void TwoNumbersAreProvided_OneIsLessThanZero_ArgumentExceptionIsThrown()
        {
            //Arrange
            var sut = new Addition();
            var num1 = -7;

            //Act + Assert
            Assert.Throws<ArgumentException>(() => sut.SetNum1(num1));
        }
    }
}
