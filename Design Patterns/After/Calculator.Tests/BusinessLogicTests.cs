﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Classes;
using Moq;
using Xunit;

namespace Calculator.Tests
{
    public class BusinessLogicTests
    {

        [Fact]
        public void MoneyIsNeeded_CalculationIsProvided_MoneyIsCorrect()
        {
            //Arrange
            var expectedResult = 15;
            var mockedCalculation = new Mock<Addition>();
            mockedCalculation.Setup(m => m.GetResult()).Returns(expectedResult);

            mockedCalculation.Verify(m => m.GetResult(), Times.AtMostOnce);

            var sut = new BusinessLogic(mockedCalculation.Object);

            //Act
            var actualResult = sut.MakeTheMoney(1, 2);
            
            Assert.Equal(expectedResult, actualResult);
        }
    }
}
