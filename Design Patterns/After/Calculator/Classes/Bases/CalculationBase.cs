﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Interfaces;

namespace Calculator.Classes.Bases
{
    public abstract class CalculationBase : ICalculation, ICalculationBuilder
    {
        private protected double? Num1 { get; set; }
        private protected double? Num2 { get; set; }

        protected CalculationBase()
        {

        }

        public ICalculation SetFirstValue(double value)
        {
            Num1 = value;
            return this;
        }
        public ICalculation SetSecondValue(double value)
        {
            Num2 = value;
            return this;
        }

        public virtual double? GetResult()
        {
            // if (Num1 == null || Num2 == null)
            //     throw new InvalidOperationException("Values have not been set");
            return PerformCalculation();
        }

        protected abstract double? PerformCalculation();

        public virtual void SetNum1(double num1)
        {
            Num1 = num1;
        }

        public virtual void SetNum2(double num2)
        {
            Num2 = num2;
        }

        public virtual ICalculation Build()
        {
            if (Num1 == null)
                throw new ArgumentNullException(nameof(Num1));
            if (Num2 == null)
                throw new ArgumentNullException(nameof(Num2));
            return this;
        }
    }
}
