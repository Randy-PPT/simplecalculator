﻿using Calculator.Interfaces;

namespace Calculator.Classes
{
    public class BusinessLogic
    {
        public BusinessLogic(ICalculation calculation)
        {
            Calculation = calculation;
        }

        private ICalculation Calculation { get; }


        public double? MakeTheMoney(double num1, double num2)
        {
            Calculation.GetResult();
            return Calculation.SetFirstValue(num1).SetSecondValue(num2).GetResult();
        }

    }
}
