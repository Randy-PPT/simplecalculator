﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Classes.Bases;
using Calculator.Interfaces;

namespace Calculator.Classes
{
    public class Addition : CalculationBase
    {
        protected override double? PerformCalculation()
        {
            return Add();
        }

        public override void SetNum1(double num1)
        {
            if (num1 < 0)
                throw new ArgumentException("Num1 cannot be less than zero");

            if (num1 == 0)
                throw new ZeroException("Num1 is zero");
            base.SetNum1(num1);
        }

        public override void SetNum2(double num2)
        {
            if (num2 % 2 == 0)
                num2 += 2;
            if (num2 < 0)
                throw new ArgumentException("Num2 cannot be less than zero");
            if (num2 == 0)
                throw new ZeroException("Num1 is zero");
            base.SetNum2(num2);
        }


        public double? Add()
        {
            return Num1 + Num2;
        }
    }
    public class Subtraction : CalculationBase
    {
        protected override double? PerformCalculation()
        {
            return Subtract();
        }

        public double? Subtract()
        {
            return Num1 - Num2;
        }
    }
    public class Division : CalculationBase
    {
        protected override double? PerformCalculation()
        {
            return Divide();
        }

        public double? Divide()
        {
            return Num1 / Num2;
        }

        public override void SetNum1(double num1)
        {
            if (num1 != 0)
                Num1 = num1;
        }

        public override void SetNum2(double num2)
        {
            if (num2 != 0)
                Num2 = num2;
        }

        public override ICalculation Build()
        {
            if (Num1 == null || Num2 == null)
                throw new DivideByZeroException("You know this isn't going to work! -_-'");
            return this;
        }
    }
    public class Multiplication : CalculationBase
    {
        protected override double? PerformCalculation()
        {
            return Multiply();
        }

        public double? Multiply()
        {
            return Num1 * Num2;
        }
    }
    public class Modulus : CalculationBase
    {
        protected override double? PerformCalculation()
        {
            return Mod();
        }

        public double? Mod()
        {
            return Num1 % Num2;
        }
    }
    public class MyFancyCalculation : CalculationBase
    {
        protected override double? PerformCalculation()
        {
            return FancyOperation();
        }

        public double? FancyOperation()
        {
            return (Num1 % Num2 + 15) * 6;
        }
    }

    public class ZeroException : Exception
    {
        public ZeroException(string message) : base(message)
        {

        }
    }
}
