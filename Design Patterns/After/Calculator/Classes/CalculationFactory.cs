﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using Calculator.Classes.Bases;
using Calculator.Interfaces;

namespace Calculator.Classes
{
    public class CalculationFactory
    {
        public string Operation { get; set; }
        public double Num1 { get; set; }
        public double Num2 { get; set; }

        public CalculationFactory(string operation, double num1, double num2)
        {
            Operation = operation;
            Num1 = num1;
            Num2 = num2;
        }

        public ICalculation GetCalculation()
        {
            //TODO: COMMENT THIS
            List<Type> allChildrenOfconcreteFactoryBase = Assembly.GetExecutingAssembly().GetTypes().Where(type => type.IsSubclassOf(typeof(ConcreteFactoryBase))).ToList();

            Type correctChildForThisOperation = allChildrenOfconcreteFactoryBase.FirstOrDefault(factory => ((ConcreteFactoryBase)Activator.CreateInstance(factory, Num1, Num2)).IsCorrectCalculation(Operation));

            if (correctChildForThisOperation != null)
                return ((ConcreteFactoryBase)Activator.CreateInstance(correctChildForThisOperation, Num1, Num2)).GetCalculation();

            throw new NotSupportedException($"Your selected operation is not supported, try implementing it.");
        }
    }

    public abstract class ConcreteFactoryBase : IHandler
    {
        protected abstract string Operation { get; }

        protected ConcreteFactoryBase Next { get; set; }

        protected ConcreteFactoryBase(double num1, double num2)
        {
            Num1 = num1;
            Num2 = num2;
        }

        public bool IsCorrectCalculation(string operation)
        {
            return operation == Operation;
        }

        private double Num1 { get; set; }
        private double Num2 { get; set; }
        protected CalculationBase Calculation { get; set; }

        public ICalculation GetCalculation()
        {
            Calculation.SetNum1(Num1);
            Calculation.SetNum2(Num2);
            return Calculation.Build();
        }

        public void SetNext(ConcreteFactoryBase next)
        {
            Next = next;
        }

        public ICalculation Handle(string operation)
        {
            Console.WriteLine($"Attempting to handle in {GetType()}");

            if (IsCorrectCalculation(operation))
            {
                Calculation.SetNum1(Num1);
                Calculation.SetNum2(Num2);
                return Calculation.Build();
            }
            return Next == null ? throw new NotImplementedException($"There was no handler defined for this operation: {operation}")
                : Next.Handle(operation);
        }
    }

    public class AdditionFactory : ConcreteFactoryBase
    {
        public AdditionFactory(double num1, double num2) : base(num1, num2)
        {
            Calculation = new Addition();
            SetNext(new SubtractionFactory(num1, num2));
        }

        protected override string Operation => "+";

    }
    public class SubtractionFactory : ConcreteFactoryBase
    {
        public SubtractionFactory(double num1, double num2) : base(num1, num2)
        {
            Calculation = new Subtraction();
            SetNext(new MultiplicationFactory(num1, num2));

        }

        protected override string Operation => "-";


    }
    public class MultiplicationFactory : ConcreteFactoryBase
    {
        public MultiplicationFactory(double num1, double num2) : base(num1, num2)
        {
            Calculation = new Multiplication();
            SetNext(new DivisionFactory(num1, num2));
        }

        protected override string Operation => "*";

    }
    public class DivisionFactory : ConcreteFactoryBase
    {
        public DivisionFactory(double num1, double num2) : base(num1, num2)
        {
            Calculation = new Division();
            SetNext(new ModulusFactory(num1, num2));
        }

        protected override string Operation => "/";

    }

    public class ModulusFactory : ConcreteFactoryBase
    {
        public ModulusFactory(double num1, double num2) : base(num1, num2)
        {
            Calculation = new Modulus();
            SetNext(new FancyFactory(num1, num2));
        }

        protected override string Operation => "%";
    }
    public class FancyFactory : ConcreteFactoryBase
    {
        public FancyFactory(double num1, double num2) : base(num1, num2)
        {
            Calculation = new MyFancyCalculation();
            SetNext(null);
        }

        protected override string Operation => "$";
    }
}
