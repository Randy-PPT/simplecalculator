﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Calculator.Interfaces
{
    public interface ICalculation
    {
        double? GetResult();
        ICalculation SetFirstValue(double value);
        ICalculation SetSecondValue(double value);
    }
}
