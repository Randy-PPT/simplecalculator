﻿namespace Calculator.Interfaces
{
    public interface ICalculationBuilder
    {
        void SetNum1(double num1);
        void SetNum2(double num2);
        ICalculation Build();
    }
}
