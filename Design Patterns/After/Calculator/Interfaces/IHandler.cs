﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Classes;

namespace Calculator.Interfaces
{
    interface IHandler
    {
        void SetNext(ConcreteFactoryBase next);
        ICalculation Handle(string request);
    }
}
