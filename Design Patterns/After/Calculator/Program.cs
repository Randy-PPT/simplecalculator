﻿using System;
using System.Reflection.Metadata;
using Calculator.Classes;
using Calculator.Interfaces;

namespace Calculator
{
    class Program
    {

        static void Main(string[] args)
        {
            ICalculation calculation = null;
            while (true)
            {
                Console.WriteLine("Please enter the first number");
                double.TryParse(Console.ReadLine(), out double num1);

                Console.WriteLine("Please enter the second number");
                double.TryParse(Console.ReadLine(), out double num2);
                while (calculation == null)
                {
                    try
                    {
                        Console.WriteLine("Please enter the operation");
                        string operation = Console.ReadLine();
                        calculation = new AdditionFactory (num1, num2).Handle(operation);

                        Console.WriteLine($"The type of calculation being performed is {calculation.GetType()}");
                        Console.WriteLine($"The result is {calculation.GetResult()}\n");
                    }
                    catch (DivideByZeroException e)
                    {
                        Console.WriteLine($"This is our special DivideByZeroException handler {e}");
                    }
                    catch (ArgumentException e)
                    {
                        Console.WriteLine($"This is our special ArgumentException handler {e}");
                    }
                    catch (NotSupportedException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }

                calculation = null;
            }
        }
    }
}
