﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Classes.Bases;

namespace Calculator.Classes
{
    public class Addition : CalculationBase
    {
        protected override double PerformCalculation()
        {
            return Add();
        }

        public double Add()
        {
            return Num1 + Num2;
        }
    }
    public class Subtraction : CalculationBase
    {
        protected override double PerformCalculation()
        {
            return Subtract();
        }

        public double Subtract()
        {
            return Num1 - Num2;
        }
    }
    public class Division : CalculationBase
    {
        protected override double PerformCalculation()
        {
            return Divide();
        }

        public double Divide()
        {
            return Num1 / Num2;
        }
    }
    public class Multiplication : CalculationBase
    {
        protected override double PerformCalculation()
        {
            return Multiply();
        }

        public double Multiply()
        {
            return Num1 * Num2;
        }
    }
    public class Modulus : CalculationBase
    {
        protected override double PerformCalculation()
        {
            return Mod();
        }

        public double Mod()
        {
            return Num1 % Num2;
        }
    }
}
