﻿using System;
using Calculator.Classes;
using Calculator.Interfaces;

namespace Calculator
{
    class Program
    {

        static void Main(string[] args)
        {
            ICalculation calculation = null;
            while (true)
            {
                Console.WriteLine("Please enter the first number");
                double.TryParse(Console.ReadLine(), out double num1);

                Console.WriteLine("Please enter the second number");
                double.TryParse(Console.ReadLine(), out double num2);
                while (calculation == null)
                {
                    try
                    {
                        Console.WriteLine("Please enter the operation");
                        string operation = Console.ReadLine();
                        calculation = SelectCalculation(operation);
                        calculation.SetValues(num1, num2);

                        Console.WriteLine($"The type of calculation being performed is {calculation.GetType()}");
                        Console.WriteLine($"The result is {calculation.GetResult()}\n");
                        operation = string.Empty;
                    }
                    catch (NotSupportedException e)
                    {
                        Console.WriteLine(e);
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                }
                calculation = null;
            }
        }

        private static ICalculation SelectCalculation(string operation)
        {
            if (operation == "+")
                return new Addition();
            if (operation == "-")
                return new Subtraction();
            if (operation == "*")
                return new Multiplication();
            if (operation == "/")
                return new Division();
            if (operation == "%")
                return new Modulus();

            throw new NotSupportedException($"Your selected operation is not supported, try implementing it.");
        }
    }
}
