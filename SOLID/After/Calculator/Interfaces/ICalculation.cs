﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Calculator.Interfaces
{
    interface ICalculation
    {
        double GetResult();
        void SetValues(double num1, double num2);
    }
}
