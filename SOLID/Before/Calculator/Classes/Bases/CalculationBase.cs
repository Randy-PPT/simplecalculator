﻿using System;
using System.Collections.Generic;
using System.Text;
using Calculator.Interfaces;

namespace Calculator.Classes.Bases
{
    public abstract class CalculationBase : ICalculation
    {
        private protected double Num1 { get; set; }
        private protected double Num2 { get; set; }

        public double GetResult()
        {
            return PerformCalculation();
        }

        public void SetValues(double num1, double num2)
        {
            Num1 = num1;
            Num2 = num2;
        }

        protected abstract double PerformCalculation();
    }
}
