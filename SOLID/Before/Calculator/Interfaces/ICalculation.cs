﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;

namespace Calculator.Interfaces
{
    public interface ICalculation
    {
        double GetResult();
        void SetValues(double num1, double num2);
    }
}
